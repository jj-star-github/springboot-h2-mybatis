package com.dingjun.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReturnResult {
    private boolean flag;
    private int code;
    private String message;
    private Object data;
}
