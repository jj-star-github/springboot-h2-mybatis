package com.dingjun.common.entity;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */

public class StatusCode {

    public static final int ok = 20001;  //成功

    public static final int error = 20000; //失败
}
