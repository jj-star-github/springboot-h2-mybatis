package com.dingjun.controller;

import com.dingjun.common.entity.ReturnResult;
import com.dingjun.common.entity.StatusCode;
import com.dingjun.dto.QueryCityDto;
import com.dingjun.pojo.City;
import com.dingjun.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    private CityService cityService;

    /**查询所有
     *
     * @return
     */
    @GetMapping("/findAllCity")
    public ReturnResult findAllCity() {
        return new ReturnResult(true, StatusCode.ok,"查询所有成功！",cityService.findAll());
    }


    /**根据id查询
     *
     * @param id
     * @return
     */
    @GetMapping("/findCityById/{id}")
    public ReturnResult findCityById(@PathVariable long id) {
        return new ReturnResult(true, StatusCode.ok,"根据id查询成功！",cityService.findCityById(id));
    }


    /**
     * 分页查询
     * @param
     * @return
     */
    @GetMapping("/pageQueryInfo")
    public ReturnResult pageQueryInfo(@RequestBody QueryCityDto queryCityDto) {
        return new ReturnResult(true, StatusCode.ok,"根据id分页查询成功！",cityService.pageQueryInfo(queryCityDto));
    }


    /**
     * 批量添加city
     * @param cityList
     * @return
     */
    @PostMapping("/batchAddCity")
    public ReturnResult batchAddCity(@RequestBody List<City> cityList) {
        return new ReturnResult(true, StatusCode.ok,"批量添加成功！",cityService.batchAddCity(cityList));
    }

    /**
     * 添加单个city
     * @param city
     * @return
     */
    @PostMapping("/addCity")
    public ReturnResult addCity(@RequestBody City city) {
        return new ReturnResult(true, StatusCode.ok,"添加单个成功！",cityService.addCity(city));
    }


    /**根据姓名模糊查询
     *
     * @return
     */
    @GetMapping("/vagueFindCity/{name}")
    public ReturnResult vagueFindCity(@PathVariable String name) {
        return new ReturnResult(true, StatusCode.ok,"根据姓名模糊查询成功！",cityService.vagueFindCity(name));
    }

}
