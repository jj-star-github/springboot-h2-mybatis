package com.dingjun.mapper;

import com.dingjun.dto.QueryCityDto;
import com.dingjun.pojo.City;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Component
@Mapper //开启注解sql的
public interface CityMapper {

    /**
     * 查询所有
     *
     * @return
     */
    List<City> findAll();

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Select("select * from city where id=#{id}")
    City findCityById(@Param("id") long id);

    /**
     * 分页查询
     * @param
     * @param
     * @param
     * @return
     */
    List<City> pageQueryInfo(@Param("queryCityDto") QueryCityDto queryCityDto,@Param("pageIndex") int pageIndex,@Param("pageSize") int pageSize);

    /**
     * 查询总数量
     *
     * @return
     */
    int queryCount();

    /**
     * 批量添加city
     *
     * @param cityList
     * @return
     */
    boolean batchAddCity(@Param("cityList") List<City> cityList);

    /**
     * 添加单个city
     *
     * @param city
     * @return
     */
    @Insert("insert into city (id,name,state,country) values (#{city.id},#{city.name},#{city.state},#{city.country})")
    boolean addCity(@Param("city") City city);


    /**根据姓名模糊查询
     *
     * @param name
     * @return
     */
    City vagueFindCity(@Param("name") String name);
}
