package com.dingjun.dto;

import com.dingjun.common.base.PageQuery;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Data
public class QueryCityDto extends PageQuery implements Serializable {

    /**
     * 根据条件id
     *//*
    private Long id;*/
    /***
     * 根据名字模糊查询
     */
    private String name;
}
