package com.dingjun.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Hotel implements Serializable {

    private Integer city;
    private String name;
    private String address;
    private String zip;

    @Override
    public String toString() {
        return "Hotel{" + "city=" + city + ", name='" + name + '\'' + ", address='" + address + '\'' + ", zip='" + zip + '\'' + '}';
    }
}
