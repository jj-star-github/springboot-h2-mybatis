package com.dingjun.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class City  implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String state;

    private String country;

    @Override
    public String toString() {
        return "City{" + "id=" + id + ", name='" + name + '\'' + ", state='" + state + '\'' + ", country='" + country + '\'' + '}';
    }
}
