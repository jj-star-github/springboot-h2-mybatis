package com.dingjun.service;

import com.dingjun.dto.QueryCityDto;
import com.dingjun.pojo.City;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
public interface CityService {

    /**
     * 查询所有
     *
     * @return
     */
    List<City> findAll();

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    City findCityById(long id);

    /**分页查询
     * @param
     * @return
     */
    PageInfo<City> pageQueryInfo(QueryCityDto queryCityDto);

    /**
     * 查询总数量
     *
     * @return
     */
    int queryCount();


    /**
     * 批量添加city
     * @param cityList
     * @return
     */
    boolean batchAddCity(List<City> cityList);


    /**
     * 添加单个city
     * @param city
     * @return
     */
    boolean addCity(City city);


    /**根据姓名模糊查询
     *
     * @param name
     * @return
     */
    City vagueFindCity(String name);
}
