package com.dingjun.service.impl;

import com.alibaba.fastjson.JSON;
import com.dingjun.dto.QueryCityDto;
import com.dingjun.mapper.CityMapper;
import com.dingjun.pojo.City;
import com.dingjun.service.CityService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Service
@Slf4j
public class CityServiceImpl implements CityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CityServiceImpl.class);

    @Autowired
    private CityMapper cityMapper;

    /**
     * 查询所有
     *
     * @return
     */
    @Override
    public List<City> findAll() {
        LOGGER.info("into method findCityById");
        return cityMapper.findAll();
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @Override
    public City findCityById(long id) {
        LOGGER.info("into method findCityById，入参：" + JSON.toJSONString(id));
        return cityMapper.findCityById(id);
    }

    /**
     * 分页查询
     * //todo 分页查询有点问题待解决
     * @param
     * @return
     */
    @Override
    public PageInfo<City> pageQueryInfo(QueryCityDto queryCityDto) {
        LOGGER.info("into method pageQueryInfo，入参：" + JSON.toJSONString(queryCityDto));
        PageHelper.startPage(queryCityDto.getPageIndex(), queryCityDto.getPageSize());
        List<City> cityList = cityMapper.pageQueryInfo(queryCityDto, queryCityDto.getPageIndex(), queryCityDto.getPageSize());
        PageInfo<City> cityPageInfo = new PageInfo<>(cityList);
        cityPageInfo.setPageNum(queryCityDto.getPageIndex());
        return cityPageInfo;
    }


    /**
     * 查询总数量
     *
     * @return
     */
    @Override
    public int queryCount() {
        return cityMapper.queryCount();
    }


    /**
     * 批量添加city
     *
     * @param cityList
     * @return
     */
    @Override
    public boolean batchAddCity(List<City> cityList) {
        LOGGER.info("into method batchAddCity，入参：" + JSON.toJSONString(cityList));
        if (!StringUtils.isEmpty(cityList)) {
            for (City city : cityList) {
                if (null != city) {
                    cityMapper.batchAddCity(cityList);
                }
            }
        }
        return true;
    }

    /**
     * 添加单个city
     *
     * @param city
     * @return
     */
    @Override
    public boolean addCity(City city) {
        LOGGER.info("into method addCity，入参：" + JSON.toJSONString(city));
        if (null != city) {
            cityMapper.addCity(city);
        }
        return true;
    }

    /**
     * 根据姓名模糊查询
     *
     * @param name
     * @return
     */
    @Override
    public City vagueFindCity(String name) {
        return cityMapper.vagueFindCity(name);
    }

}
